package com.citiestest.ui.map;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.citiestest.SchedulersFacade;
import com.citiestest.data.City;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created on 05.02.2018.
 */

public class MapViewModel extends ViewModel {

    private final SchedulersFacade mSchedulersFacade;

    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private final MutableLiveData<City> mResponse = new MutableLiveData<>();

    @Inject
    public MapViewModel(SchedulersFacade schedulersFacade) {
        mSchedulersFacade = schedulersFacade;
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.clear();
    }

    MutableLiveData<City> response() {
        return mResponse;
    }

    void simpleAction(City city) {
        mResponse.postValue(city);
    }
}
