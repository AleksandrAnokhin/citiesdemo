package com.citiestest.data.source;

import android.support.annotation.NonNull;

import com.citiestest.data.City;
import com.citiestest.data.source.local.CitiesLocalDataSource;
import com.citiestest.data.source.remote.CitiesRemoteDataSource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created on 20.12.17.
 */

@Singleton
public class CitiesRepository implements CitiesDataSource {

    private final CitiesLocalDataSource citiesLocalDataSource;

    private final CitiesRemoteDataSource citiesRemoteDataSource;

    @Inject
    public CitiesRepository(@NonNull CitiesLocalDataSource citiesLocalDataSource,
                            @NonNull CitiesRemoteDataSource citiesRemoteDataSource) {
        this.citiesLocalDataSource = citiesLocalDataSource;
        this.citiesRemoteDataSource = citiesRemoteDataSource;
    }

    @Override
    public Observable<List<City>> getCities() {
        return citiesRemoteDataSource.getCities();
    }
}
